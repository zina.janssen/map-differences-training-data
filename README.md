# Description
Project name: Evaluate land cover map differences caused by training data  
Author: Zina Janssen  
Date: August 2022 - April 2023  
Description: this repository contains the scripts used for my thesis for the masters program Geo Information Science at
Wageningen University and Research

This project creates multiple land cover maps through a random forest based on different training data sets. By evaluating the differences between these land cover, insight into the impact training data has on land cover map output is improved. The map differences found are then compared to differences resulting from differences in remote sensing input data. By comparing the extent to which both of these alterations influence map outputs, both impacts can be contextualized.

#### Required data and sources
EEA grid for parellizing classification https://github.com/NINAnor/ELC10/tree/main/DATA/For_GEE  
DW training data from https://download.pangaea.de/dataset/933475/allfiles.zip  
GLC training data from http://data.ess.tsinghua.edu.cn/  

All satellite data is automatically loaded in the GEE script  
LUCAS data is downloaded automatically through the Rscript  
CGLS data is not freely avaliable  

# Workflow
Prior to using the GEE scripts, the training data needs to be prepared  
0. Training data preparations  
LUCAS data:   
1. 1_out_createLUCSATD.R - this script loads, subsets the LUCAS data
2. Upload these to GEE  
CGLS data:   
1. 1_out_createExtendCGLSTD.R - this script subsets and harmonizes CGLS data. From these points, buffers are screated from wich extra training points are created
2. Load these into GEE
3. 1_in_reduceCGLS.js - samples ESA land cover data to CGLS training points and uploads these points to drive
Locally store these points 
4. 1_out_ESAcorrectCGLS.R - this script removes the CGLS point that do not correspond to the ESA labels
5. Upload points to GEE  
DW data:  
1. 1_in_selectDWtiles.R - this script selects and transforms raster tiles that fall within the AOI
2. 1_out_createDWTD.R - this script samples points from the selected points and harmonizes land cover classes
3. upload these points to GEE

1. Creating land cover maps
This workflow is the same for all scripts  
1. 1_out_<Training_data>Classification.js - these scripts load the training data and creates land cover maps for spain
2. 1_out_createVisualisations.ipynb - this script makes maps of all land cover maps and saves them  

2. Evaluating differences
The evaluation of differences consists of comparing class areas, spatial consistency, entropy, and confusion matrices  
Class area  
1. 2_out_classAreas.js - this script calculates class area and upload the table to drive  
2. 2_out_areaTables.js - this scrip downloads the tables from drive and creates a complete table   
Spatial consistency  
1. 2_out_spatialOverlay.js - this script overlays the maps and calculates where maps agree and disagree  
Confusion matrices  
1. 2_out_confusionMaps.js - combine map pixels st classes of both maps are captured. From these a confusion matrix can be made  
Entropy  
1. 2_in_FROMGCL.R - this map combines all tiles and masks it to create a complete land cover map
2. upload this land cover map to GEE
3. 2_out_compareMapEntropy.js - this script creates entropy maps per land cover map and saves them  
4. 1_out_createVisualisations.ipynb - use this script to create entropy maps
5. 2_out_entropyStatistics.ipynb - calculate the entropy per spatial consistency class, and calculate entropy distributions

3. Comparing differences  
Same steps as for the above maps
