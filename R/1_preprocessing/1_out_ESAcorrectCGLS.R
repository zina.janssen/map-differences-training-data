# Title: Reduce the CGLS data set based on ESA
# Name: Zina Janssen
# create: 14/03
# this script reads the CGLS points with the added ESA classes and removes the CGLS point
# where the classes do not overlap

#### 1 Initializing ####
source('../funcs/0_checkLoadPackages.R')
source('../funcs/1_out_classFrequencyTable.R')

# load all packages
listOfPackages <- c("sf","dplyr","purrr","terra")
lapply(listOfPackages, checkInstall) -> output
# create output dir and load input dit
CGLSext <- "1_in_ESACGLS"
CGLSoutputs <- "../../CGL-100/1_out_CGLS"
if (!dir.exists(CGLSoutputs)) {dir.create(CGLSoutputs)}

# load the complete data set and the shp with which the thing can be subset
filename<-file.path(CGLSoutputs,'CGLSwithESA.shp')
CGLS <- st_read(filename)
# now change all the landcover labels
# snow and ice and mangroves were not present so these changes were not made
CGLS[CGLS$Map == 10,]$Map <- 3
CGLS[CGLS$Map == 20,]$Map <- 4
CGLS[CGLS$Map == 30,]$Map <- 5
CGLS[CGLS$Map == 40,]$Map <- 2
CGLS[CGLS$Map == 50,]$Map <- 1
CGLS[CGLS$Map == 60,]$Map <- 6
#CGLS[CGLS$Map == 70,]$Map <- 6
CGLS[CGLS$Map == 80,]$Map <- 7
CGLS[CGLS$Map == 90,]$Map <- 8

# select the columns of CGLS which are equal to the ESA map
CGLS$equal <- CGLS$LC1_lbl == CGLS$Map
equal<-CGLS[which(CGLS$equal),]

# save the first reduced version
st_write(equal,file.path(CGLSoutputs,"1_out_CGLSESAcorrected.shp"),append=F)
classFreqTable(equal$LC1_lbl,file.path(CGLSoutputs,"1_out_CGLSClassFreqExt.csv"))
