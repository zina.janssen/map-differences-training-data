# Title: reproject all rasters to common resolution
# Name: Zina Janssen
# Created: 20/09
## Description: this function takes a list of rasters and all transforms them
# to a common resolution

transformRaster <- function(rasterL,filename,proj="epsg:4326"){
  # load the inner part as the raster
  ras <- rasterL[[1]]
  # reproject the raster to the required proj
  ras<- terra::project(ras,proj)
  # create the output filename
  outfilename<-unlist(strsplit(filename,"/"))[7]
  outfilepath<-file.path("1_int_Reprojected",outfilename)
  # save the projected raster
  terra::writeRaster(ras,filename=outfilepath,overwrite=T)
  }

rasterTransform <- function(num,foldNameStart,expert=F){
  # load folder name depending on whether it is needed to look at expert or non expert files
  if (expert){foldname <- paste0(foldNameStart,"Experts_tiles/")}
  else {foldname <- paste0(foldNameStart,"Non_expert_tiles/")}
  
  # make a list of all the rasters  
  listname <- paste0(foldname,as.character(num))
  filelist <- list.files(listname,pattern = glob2rx("*.tif"),full.names = T)
  # remove incorrect filenames from namelist
  fileListNames <- filelist[!grepl(pattern = "/$",filelist)]
  # read them as rast
  allrasters <- lapply(fileListNames, rast)
  # now transform and write all rasers
  list(ras=allrasters,filename=filelist) %>%
    purrr::pmap(transformRaster)}
